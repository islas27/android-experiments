package com.jonathan.solitairegame;

/**
 * Created by jonathan on 5/5/16.
 */

import android.app.Activity;
import android.os.Bundle;

public class About extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
    }
}
package com.jonathan.myportfolio;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.jonathan.myportfolio.project.ProjectItem;

public class ProjectsActivity extends AppCompatActivity implements OnListFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onListFragmentInteraction(ProjectItem item) {
        System.out.println(item);

        getFragmentManager().beginTransaction().replace(R.id.content, detailFragment.).commit();
        Toast.makeText(this, "miau",Toast.LENGTH_SHORT).show();
    }

    public void createProject(View view){
        //load fragment
        Toast.makeText(this, "I should load the creation fragment", Toast.LENGTH_SHORT).show();
    }


}

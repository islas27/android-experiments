package com.jonathan.myportfolio.project;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jonathan on 5/12/16.
 */

public class ProjectContent {
    /**
     * An array of sample (dummy) items.
     */
    public static final List<ProjectItem> ITEMS = new ArrayList<ProjectItem>();

    private static void addItem(ProjectItem item) {
        ITEMS.add(item);
    }

    private static final int COUNT = 3;

    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createDummyItem(i));
        }
    }

    private static ProjectItem createDummyItem(int position) {
        return new ProjectItem(position, "project"+position, "description", "git url", "2016");
    }
}
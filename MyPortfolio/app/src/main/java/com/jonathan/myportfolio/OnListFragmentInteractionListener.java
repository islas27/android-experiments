package com.jonathan.myportfolio;

import com.jonathan.myportfolio.project.ProjectItem;

/**
 * Created by jonathan on 5/12/16.
 */

public interface OnListFragmentInteractionListener {
    void onListFragmentInteraction(ProjectItem item);
}

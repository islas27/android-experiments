package com.jonathan.myportfolio.project;

/**
 * Created by jonathan on 5/12/16.
 */
public class ProjectItem {
    public int id;
    public String name;
    public String description;
    public String url;
    public String yearDevelopment;

    public ProjectItem(int id, String name, String description, String url, String yearDevelopment) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.url = url;
        this.yearDevelopment = yearDevelopment;
    }

    public ProjectItem(String name, String description, String url, String yearDevelopment) {
        this.name = name;
        this.description = description;
        this.url = url;
        this.yearDevelopment = yearDevelopment;
    }

    @Override
    public String toString(){
        return name + ": " + yearDevelopment;
    }
}

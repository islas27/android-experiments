package com.jonathan.portfolio2;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by jonathan on 5/18/16.
 */

public class DBHelper extends SQLiteOpenHelper{
    //each time if you Add, Edit table, you need to change the
    //version number.
    private static final int DATABASE_VERSION = 4;

    // Database Name
    private static final String DATABASE_NAME = "crud.db";

    public DBHelper(Context context ) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //All necessary tables you like to create will create here

        String CREATE_TABLE_STUDENT = "CREATE TABLE " + Project.TABLE  + "("
                + Project.KEY_ID  + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + Project.KEY_name + " TEXT, "
                + Project.KEY_description + " TEXT, "
                + Project.KEY_year + " INTEGER, "
                + Project.KEY_url + " TEXT )";
        System.out.println(CREATE_TABLE_STUDENT);
        db.execSQL(CREATE_TABLE_STUDENT);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed, all data will be gone!!!
        db.execSQL("DROP TABLE IF EXISTS " + Project.TABLE);
        // Create tables again
        onCreate(db);

    }
}

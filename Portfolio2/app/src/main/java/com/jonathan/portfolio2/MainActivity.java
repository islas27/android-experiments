package com.jonathan.portfolio2;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jonathan on 5/18/16.
 */

public class MainActivity extends ListActivity implements android.view.View.OnClickListener {

    Button btnAdd,btnGetAll;
    TextView project_Id;
    @Override
    public void onClick(View view) {
        if (view== findViewById(R.id.btnAdd)){
            Intent intent = new Intent(this,ProjectDetail.class);
            intent.putExtra("project_Id",0);
            startActivity(intent);
        }else {
            ProjectDAO dao = new ProjectDAO(this);
            ArrayList<HashMap<String, String>> projectList =  dao.getProjectList();
            if(projectList.size()!=0) {
                ListView lv = getListView();
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent,
                                            View view,int position, long id) {
                        project_Id = (TextView) view.findViewById(R.id.projectId);
                        String projectId = project_Id.getText().toString();
                        Intent objIndent = new Intent(getApplicationContext(),ProjectDetail.class);
                        objIndent.putExtra("project_Id", Integer.parseInt( projectId));
                        startActivity(objIndent);
                    }
                });
                ListAdapter adapter = new SimpleAdapter( MainActivity.this,projectList,
                        R.layout.view_project_entry,
                        new String[] { "id","name"},
                        new int[] {R.id.projectId, R.id.projectName});
                setListAdapter(adapter);
            }else{
                Toast.makeText(this,"No project!",Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);
        btnGetAll = (Button) findViewById(R.id.btnGetAll);
        btnGetAll.setOnClickListener(this);
    }
}
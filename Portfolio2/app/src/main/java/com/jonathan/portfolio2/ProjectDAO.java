package com.jonathan.portfolio2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jonathan on 5/18/16.
 */
public class ProjectDAO {
    private DBHelper dbHelper;

    public ProjectDAO(Context context) {
        dbHelper = new DBHelper(context);
    }

    public int insert(Project project) {
        //Open connection to write data
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Project.KEY_year, project.year);
        values.put(Project.KEY_url, project.url);
        values.put(Project.KEY_description,project.description);
        values.put(Project.KEY_name, project.name);
        // Inserting Row
        long project_Id = db.insert(Project.TABLE, null, values);
        db.close(); // Closing database connection
        return (int) project_Id;
    }

    public void delete(int project_Id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // It's a good practice to use parameter ?, instead of concatenate string
        db.delete(Project.TABLE, Project.KEY_ID + "= ?", new String[] { String.valueOf(project_Id) });
        db.close(); // Closing database connection
    }

    public void update(Project project) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Project.KEY_year, project.year);
        values.put(Project.KEY_url, project.url);
        values.put(Project.KEY_description,project.description);
        values.put(Project.KEY_name, project.name);
        // It's a good practice to use parameter ?, instead of concatenate string
        db.update(Project.TABLE, values, Project.KEY_ID + "= ?", new String[] { String.valueOf(project.projectId) });
        db.close(); // Closing database connection
    }

    public ArrayList<HashMap<String, String>> getProjectList() {
        //Open connection to read only
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery =  "SELECT  " +
                Project.KEY_ID + "," +
                Project.KEY_name + "," +
                Project.KEY_description + "," +
                Project.KEY_year + "," +
                Project.KEY_url +
                " FROM " + Project.TABLE;
        //Project project = new Project();
        ArrayList<HashMap<String, String>> projectList = new ArrayList<HashMap<String, String>>();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> project = new HashMap<String, String>();
                project.put("id", cursor.getString(cursor.getColumnIndex(Project.KEY_ID)));
                project.put("name", cursor.getString(cursor.getColumnIndex(Project.KEY_name)));
                projectList.add(project);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return projectList;
    }

    public Project getProjectById(int Id){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery =  "SELECT  " +
                Project.KEY_ID + "," +
                Project.KEY_name + "," +
                Project.KEY_description + "," +
                Project.KEY_year + "," +
                Project.KEY_url +
                " FROM " + Project.TABLE
                + " WHERE " +
                Project.KEY_ID + "=?";// It's a good practice to use parameter ?, instead of concatenate string
        int iCount =0;
        Project project = new Project();
        Cursor cursor = db.rawQuery(selectQuery, new String[] { String.valueOf(Id) } );
        if (cursor.moveToFirst()) {
            do {
                project.projectId =cursor.getInt(cursor.getColumnIndex(Project.KEY_ID));
                project.name =cursor.getString(cursor.getColumnIndex(Project.KEY_name));
                project.description  =cursor.getString(cursor.getColumnIndex(Project.KEY_description));
                project.year =cursor.getInt(cursor.getColumnIndex(Project.KEY_year));
                project.url =cursor.getString(cursor.getColumnIndex(Project.KEY_url));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return project;
    }
}

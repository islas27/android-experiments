package com.jonathan.portfolio2;

/**
 * Created by jonathan on 5/18/16.
 */
public class Project {
    //Table name
    public static final String TABLE = "Project";

    //Table Columns names
    public static final String KEY_ID = "id";
    public static final String KEY_name = "name";
    public static final String KEY_description = "description";
    public static final String KEY_year = "year_development";
    public static final String KEY_url = "git_url";

    //Properties
    public int projectId;
    public String name;
    public String description;
    public int year;
    public String url;
}

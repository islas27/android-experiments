package com.jonathan.portfolio2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by jonathan on 5/18/16.
 */

public class ProjectDetail extends AppCompatActivity implements android.view.View.OnClickListener{

    Button btnSave, btnDelete, btnClose;
    EditText editTextName;
    EditText editTextDescription;
    EditText editTextYear;
    EditText editTextUrl;
    private int _Project_Id =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        btnClose = (Button) findViewById(R.id.btnClose);
        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextDescription = (EditText) findViewById(R.id.editTextDescription);
        editTextYear = (EditText) findViewById(R.id.editTextYearD);
        editTextUrl = (EditText) findViewById(R.id.editTextUrl);
        btnSave.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnClose.setOnClickListener(this);
        _Project_Id =0;
        Intent intent = getIntent();
        _Project_Id =intent.getIntExtra("project_Id", 0);
        ProjectDAO dao = new ProjectDAO(this);
        Project project;
        project = dao.getProjectById(_Project_Id);
        editTextYear.setText(String.valueOf(project.year));
        editTextName.setText(project.name);
        editTextDescription.setText(project.description);
        editTextUrl.setText(project.url);
    }

    public void onClick(View view) {
        if (view == findViewById(R.id.btnSave)){
            ProjectDAO repo = new ProjectDAO(this);
            Project project = new Project();
            project.url = editTextUrl.getText().toString();
            project.year= Integer.parseInt(editTextYear.getText().toString());
            project.description= editTextDescription.getText().toString();
            project.name=editTextName.getText().toString();
            project.projectId= _Project_Id;

            if (_Project_Id ==0){
                _Project_Id = repo.insert(project);
                Toast.makeText(this,"New Project Insert",Toast.LENGTH_SHORT).show();
            }else{
                repo.update(project);
                Toast.makeText(this,"Project Record updated",Toast.LENGTH_SHORT).show();
            }
        }else if (view== findViewById(R.id.btnDelete)){
            ProjectDAO repo = new ProjectDAO(this);
            repo.delete(_Project_Id);
            Toast.makeText(this, "Project Record Deleted", Toast.LENGTH_SHORT).show();
            finish();
        }else if (view== findViewById(R.id.btnClose)){
            finish();
        }
    }
}
